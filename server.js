var express = require('express');

var talks = require('./routes/talks');

var app = express();

app.configure( function() {
	app.use(express.logger('dev'));		/* default, short, tiny, dev */
	app.use(express.bodyParser());
});

app.all('/*', function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	// res.header("Access-Control-Allow-Methods", "GET, POST","PUT");
	//res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS');
	//res.header("Access-Control-Allow-Headers", '*, Accept, Origin, X-Requested-With, X-Prototype-Version, X-CSRF-Token, Content-Type');
	next();
});


app.get('/talks', talks.findAll);
app.get('/talk/:id', talks.findById);
app.post('/talk', talks.addTalk);
app.put('/talk/:id', talks.updateTalk);
app.delete('/talk/:id', talks.deleteTalk);


app.listen(3000);
console.log('Listening on port 3000...');
