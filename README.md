
# Simple RESTFull webservices for talks management

You can test it with cURL

* Get all talks:

curl -i -X GET http://localhost:3000/talks

* Get talk with _id value of 5260210cc7aeec483a000002 (use a value that exists in your database):

curl -i -X GET http://localhost:3000/talk/5260210cc7aeec483a000002

* Delete talk with _id value of 5260210cc7aeec483a000002:

curl -i -X DELETE http://localhost:3000/talk/5260210cc7aeec483a000002

* Add a new talk:

curl -i -X POST -H 'Content-Type: application/json' -d "{ \"name\" : \"Reactive programming with Meteor\", \"author\" : \"Neto\"}"  http://localhost:3000/talk

* Modify talk with _id value of 5260210cc7aeec483a000002:

curl -i -X PUT -H 'Content-Type: application/json' -d  "{ \"name\" : \"Meteor 101\", \"author\" : \"Neto\"}" http://localhost:3000/talk/52602fe4c9cd96c842000001
