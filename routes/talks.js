
var mongo = require('mongodb');
var ObjectID = require('mongodb').ObjectID;

var Server = mongo.Server,
	Db = mongo.Db,
	BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {auto_reconnect:true});
db = new Db('talkdb', server);

db.open(function(err, db) {
	if (!err) {
		console.log("Connected to 'talkdb' database");
		db.collection('talks', {strict:true}, function(err, collection) {
			if (err)  {
				console.log("The 'talks' collection doesn't exist. Creating it with sample data...");
				populateDB();
			}
		});
	}
});

exports.findAll = function(req, res) {
	db.collection('talks', function(err, collection) {
		collection.find().toArray( function(err, items) {
			res.send(items);
		});
	});
}

exports.findById = function(req, res) {
	var id = req.params.id;
	console.log('Retrieving talk: ' + id);
	db.collection('talks', function(err, collection) {
		collection.findOne( { '_id' : ObjectID.createFromHexString(id)}, function (err, item) {
			res.send(item);
		});
	});
	
}


exports.addTalk = function(req, res) {
	var talk = req.body;
	console.log('Adding talk: ' + JSON.stringify(talk));
	db.collection('talks', function(err, collection) {
		collection.insert(talk, {safe:true}, function(err, result) {
			if (err) {
				res.send({'error':'An error has occurred'});
			} else {
				console.log('Success: ' + JSON.stringify(result[0]));
				res.send(result[0]);
			}
		});
	});
}

exports.updateTalk = function(req, res) {
    var id = req.params.id;
    var talk = req.body;
    console.log('Updating talk: ' + id);
    console.log(JSON.stringify(talk));
    db.collection('talks', function(err, collection) {
        collection.update({'_id':new BSON.ObjectID(id)}, talk, {safe:true}, function(err, result) {
            if (err) {
                console.log('Error updating talk: ' + err);
                res.send({'error':'An error has occurred'});
            } else {
                console.log('' + result + ' document(s) updated');
                res.send(talk);
            }
        });
    });
}


exports.deleteTalk = function(req, res) {
    var id = req.params.id;
    console.log('Deleting talk: ' + id);
    db.collection('talks', function(err, collection) {
        collection.remove({'_id':new BSON.ObjectID(id)}, {safe:true}, function(err, result) {
            if (err) {
                res.send({'error':'An error has occurred - ' + err});
            } else {
                console.log('' + result + ' document(s) deleted');
                res.send(req.body);
            }
        });
    });
}

var populateDB = function() {

	var talks = [
		{name:'M.E.A.N.', author: 'Neto'}, 
		{name:'SOLR', author:'Renzi'},
		{name:'AWS', author:'Kenji'}
	];

	db.collection('talks', function(err, collection) {
		collection.insert(talks, {safe:true}, function(err, result) {});
	});
 
};